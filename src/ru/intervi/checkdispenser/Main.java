package ru.intervi.checkdispenser;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.Material;
import org.bukkit.block.Dispenser;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin implements Listener {
	private boolean print = false;
	private CommandSender cmdsender = null;
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	public static boolean itemEquals(ItemStack item1, ItemStack item2) {
		//ItemStack.equalse and ItemStack.isSimilar - is shit
		if (item1 == null || item2 == null) return false;
		if (!item1.getType().equals(item2.getType())) return false;
		if (item1.getDurability() != item2.getDurability()) return false;
		if (item1.hasItemMeta() != item2.hasItemMeta()) return false;
		if (item1.hasItemMeta() && !item1.getItemMeta().equals(item2.getItemMeta())) return false;
		if (!item1.getEnchantments().equals(item2.getEnchantments())) return false;
		return true;
	}
	
	public static int getSlot(Inventory inv, ItemStack item) {
		for (int i = 0; i < inv.getSize(); i++) {
			ItemStack is = inv.getItem(i);
			if (itemEquals(item, is)) return i;
		}
		return -1;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDispense(BlockDispenseEvent event) {
		if (print) {
			cmdsender.sendMessage("======= BlockDispenserHandlers =======");
			for (RegisteredListener listener : event.getHandlers().getRegisteredListeners()) {
				String msg = '[' + listener.getPriority().toString() + "] " + listener.getPlugin().getName();
				if (cmdsender != null) cmdsender.sendMessage(msg);
				else System.out.println(msg);
			}
		}
		if (!(event.getBlock().getState() instanceof Dispenser)) return;
		Dispenser disp = ((Dispenser) event.getBlock().getState());
		new Run(disp, event.getItem(), getSlot(disp.getInventory(), event.getItem())).runTaskLater(this, 1);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission("checkdispenser.check")) {
			print = !print;
			if (print) {
				cmdsender = sender;
				sender.sendMessage("Print plugins on.");
			} else {
				cmdsender = null;
				sender.sendMessage("Print plugins off.");
			}
		} else sender.sendMessage("not permission");
		return true;
	}
	
	private class Run extends BukkitRunnable {
		public Run(Dispenser disp, ItemStack item, int slot) {
			DISP = disp;
			ITEM = item;
			SLOT = slot;
		}
		
		private final Dispenser DISP;
		private final ItemStack ITEM;
		private final int SLOT;
		
		@Override
		public void run() {
			Inventory inv = DISP.getInventory();
			int slot = getSlot(inv, ITEM);
			if (slot == -1 || slot == SLOT) return;
			if (itemEquals(inv.getItem(slot), ITEM)) {
				inv.setItem(slot, new ItemStack(Material.AIR));
				DISP.update(true, true);
			}
		}
	}
}
