# CheckDispenser

Fix dupe armor with dispenser. Dupe: put armor to dropper, push button, armor wear to the player and remains in dropper. Need ClearLag (2.7.9?) or other strange plugin.

## Commands

* **/chdisp** on/off print plugin handle *BlockDispenseEvent*

## Permissions

* **checkdispenser.check** - for use */chdisp*
